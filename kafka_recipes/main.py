import json

import click

from kafka_recipes.kafka_utils import connect_kafka_consumer
from kafka_recipes.recipes import get_recipes
from kafka_recipes.recipes import parse_recipe
from kafka_recipes.recipes import SALADS_URL
from kafka_recipes.kafka_utils import connect_kafka_producer
from kafka_recipes.kafka_utils import publish_message
from kafka_recipes.utils import limit


DEFAULT_GROUP_ID = 'python-client'


@click.group()
def run():
    pass


@run.command()
@click.option('--url', type=str, default=SALADS_URL, show_default=True)
@click.option('--topic', type=str, required=True)
@click.option('--max-count', type=int, default=None)
@click.option('--delay', type=float, default=0, show_default=True)
def download_raw_recipes(
        url,
        topic,
        max_count,
        delay
):
    print(f"Running producer: downloading raw recipes from {url}")
    with connect_kafka_producer() as producer:
        for recipe in limit(get_recipes(url), max_count, delay):
            publish_message(
                producer,
                topic_name=topic,
                key='raw',
                value=recipe
            )


@run.command()
@click.option('--source-topic', type=str, required=True)
@click.option('--target-topic', type=str, required=True)
@click.option('--group-id', type=str, default=DEFAULT_GROUP_ID, show_default=True)
@click.option('--max-count', type=int, default=None)
@click.option('--delay', type=float, default=0, show_default=True)
@click.option('--timeout', type=float, default=60, show_default=True)
def parse_raw_recipes(
        source_topic,
        target_topic,
        group_id,
        max_count,
        delay,
        timeout
):
    print(f"Running consumer: parsing raw recipes")
    with connect_kafka_consumer(source_topic, group_id, timeout) as consumer:
        with connect_kafka_producer() as producer:

            for msg in limit(consumer, max_count, delay):

                try:
                    print(f"Processing msg offset={msg.offset} ...")
                    recipe_data = parse_recipe(html=msg.value.decode('UTF-8'))
                except Exception as exc:
                    print(f"Failed to process msg offset={msg.offset}")
                    print(exc)
                else:
                    publish_message(
                        producer,
                        topic_name=target_topic,
                        key='parsed',
                        value=json.dumps(recipe_data)
                    )

                consumer.commit()


@run.command()
@click.option('--topic', type=str, required=True)
@click.option('--calories-threshold', type=int, required=True)
@click.option('--group-id', type=str, default=DEFAULT_GROUP_ID, show_default=True)
@click.option('--max-count', type=int, default=None)
@click.option('--delay', type=float, default=0, show_default=True)
@click.option('--timeout', type=float, default=60, show_default=True)
def alert_calories(
        topic,
        calories_threshold,
        group_id,
        max_count,
        delay,
        timeout
):
    with connect_kafka_consumer(topic, group_id, timeout) as consumer:
        for msg in limit(consumer, max_count, delay):
            record = json.loads(msg.value.decode('UTF-8'))
            calories = record['calories']
            if calories > calories_threshold:
                title = record['title']
                print(f"Alert: Recipe {title!r} has {calories} calories")


if __name__ == '__main__':
    run()
