import contextlib

from kafka import KafkaConsumer
from kafka import KafkaProducer


def _format_value(value, max_length=60):
    if len(value) <= max_length:
        return repr(value)
    else:
        return f"({len(value)} bytes)"


def publish_message(producer_instance, topic_name, key, value):
    key_bytes = str(key).encode('UTF-8')
    value_bytes = str(value).encode('UTF-8')

    print(f"Publishing to {topic_name}: {key!r}={_format_value(value_bytes)}")
    producer_instance.send(
        topic_name,
        key=key_bytes,
        value=value_bytes
    )
    producer_instance.flush()


BOOTSTRAP_SERVERS = ['localhost:9092']

@contextlib.contextmanager
def connect_kafka_producer():
    producer = KafkaProducer(
        bootstrap_servers=BOOTSTRAP_SERVERS,
    )
    yield producer

    producer.close()


@contextlib.contextmanager
def connect_kafka_consumer(topic_name, group_id=None, timeout=10.0):
    consumer = KafkaConsumer(
        topic_name,
        group_id=group_id,
        auto_offset_reset='earliest',
        bootstrap_servers=BOOTSTRAP_SERVERS,
        consumer_timeout_ms=int(timeout * 1000),
    )

    yield consumer

    consumer.close(autocommit=True)
