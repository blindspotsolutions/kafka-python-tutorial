import json

import requests
from bs4 import BeautifulSoup

from kafka_recipes.utils import limit


def fetch_raw(url):
    print(f"Fetching {url}")

    r = requests.get(url)
    if r.status_code == 200:
        return r.text.strip()
    else:
        return ""


def fetch_links(url, selector):
    print(f"Extracting links from {url} with selector {selector}")
    r = requests.get(url)
    if r.status_code == 200:
        soup = BeautifulSoup(r.text, 'lxml')
        return (a['href'] for a in soup.select(selector))
    else:
        raise Exception(f"failed to fetch {url}: {r.status_code}")


def get_recipes(url):
    return (
        fetch_raw(link)
        for link in fetch_links(url, 'div.card__detailsContainer a.card__titleLink')
    )


def parse_recipe(html):
    soup = BeautifulSoup(html, 'lxml')

    def load_text(selector, attribute=None):
        sections = soup.select(selector)
        if not sections:
            raise Exception(f"section at {selector!r} not found!")

        section = sections[0]
        if attribute:
            return str(section.get(attribute)).strip()
        else:
            return section.text.strip()

    title = load_text('h1.heading-content')
    link = load_text('head link[rel="canonical"]', attribute='href')
    submit_by = load_text('a.author-name')
    description = load_text('div.recipe-summary p').replace('"', '')
    calories = parse_calories(load_text('div.recipe-nutrition-section div.section-body'))
    ingredients = [
        ingredient.text.strip()
        for ingredient in soup.select('ul.ingredients-section span.ingredients-item-name')
    ]

    return {
        'title': title,
        'link': link,
        'submitter': submit_by,
        'description': description,
        'calories': calories,
        'ingredients': [
            {'step': step}
            for step in ingredients
        ]
    }


def parse_calories(text):
    """
    26.9 calories; protein 1.1g 2% DV; carbohydrates 6.6g 2% DV; fat 0.1g; ...
    """
    calories_part = text.split("; ")[0]
    assert calories_part.endswith(" calories")
    return float(calories_part.split(" ")[0])


SALADS_URL = 'https://www.allrecipes.com/recipes/96/salad/'

if __name__ == '__main__':
    for html in limit(get_recipes(SALADS_URL), 3, 2000):
        # print(html)
        # print(parse_recipe(html))
        print(json.dumps(parse_recipe(html), indent="  "))
