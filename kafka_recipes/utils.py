import time


def limit(g, max_count=None, delay=None):
    if max_count == 0:
        return

    for index, item in enumerate(g):
        yield item

        if max_count is not None and (index + 1) >= max_count:
            return

        if delay:
            time.sleep(delay)
