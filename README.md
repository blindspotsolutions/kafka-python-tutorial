# Kafka-Python Tutorial

In order to accustomize myself with Kafka and developing for Kafka in Python, I made this little toy project.
Hopefully someone else will find it useful while learning Kafka.


## 1. Understanding Kafka

What did I read to get here?

- [Apache Kafka](https://en.wikipedia.org/wiki/Apache_Kafka) on Wikipedia — basic info
- [Introduction to Kafka](http://kafka.apache.org/intro#intro_topics) by Apache — extensive overview of basic terminology
- [Getting started with Apache Kafka in Python
](https://towardsdatascience.com/getting-started-with-apache-kafka-in-python-604b3250aa05) tutorial — this project is based on this tutorial, however the source codes were modified extensively
- [Kafka Tool](https://www.kafkatool.com/) — useful GUI tool (warning: horrible webpage)


## 2. Run Kafka server on local machine

### 2.1. macOS

1. Install required stuff using [Homebrew](https://brew.sh/)
    - `brew install kafka`
    - `brew cask install kafka-tool` (optional)
2. Checkout this project and `cd` into its dir
3. Run [Zookeeper](https://en.wikipedia.org/wiki/Apache_ZooKeeper) (installed by Homebrew as dependency for `kafka`) 
    - first adjust [configuration](config/zookeeper.properties) to your needs (default values should do)
    - `zookeeper-server-start config/zookeeper.properties`
4. Run Kafka
    - first adjust [configuration](config/server.properties) to your needs (default values should do)
    - `kafka-server-start config/server.properties`

Make sure it's running by opening two terminals with following commands:

1. `kafka-console-producer --broker-list localhost:9092 --topic test`
2. `kafka-console-consumer --bootstrap-server localhost:9092 --topic test --from-beginning`

Now send messages/events into the first terminal (producer) and receive it in the second (consumer).

Optionally connect to your server using *Kafka Tool*:

- specify connection as `localhost:2181`
- clicking on Clusters/local/Topics/**test**
- click the green button to download data for the selected topic
- in order to see data as plaintext, click *Properties* tab, select both *Key* and *Message* as *String*, the click *Refresh*


## 3. Playing with *kafka-recipes* app

### 3.1. App overview

This app has three parts:

1. **Data-mining** (Producer *A*): Download recipe pages from [allrecipes.com](https://www.allrecipes.com/recipes/96/salad/) and stores them as raw HTML into Kafka topic *A*.
2. **Transformation** (Consumer *A* & Producer *B*): Extract data from raw HTML in topic *A* and store it as JSON into topic *B* (e.g. *A*=`raw` to *B*=`parsed`).
3. **Monitoring** (Consumer *B*): Go through data in topic *B* and write an alert message if the recipe has more than X calories.


The [main.py](kafka_recipes/main.py) file contains high-level commands for running these three parts.
Actual "business logic" is in [recipes.py](kafka_recipes/recipes.py) and technical Kafka stuff is in [kafka_utils.py](kafka_recipes/kafka_utils.py). 

### 3.2. Parsed JSON example

As stored in topic *B*

```json
{
    "title": "Cucumber Salad with Dill Vinaigrette",
    "link": "https://www.allrecipes.com/recipe/230082/cucumber-salad-with-dill-vinaigrette/",
    "submitter": "CHEFBOYOSARAH",
    "description": "A fresh and light salad that involves no mayo or sour cream.",
    "calories": 193.0,
    "ingredients": [
        {"step": "1 pint grape tomatoes, halved"},
        {"step": "3 medium (blank)s cucumbers - peeled, seeded, and chopped"}
    ]
}
```

### 3.3. Running the app

(Tested on Python 3.6)

#### 3.3.1. Install requirements

- `pip3 install -r requirements.txt`

- [kafka-python](https://pypi.org/project/kafka-python/) for Kafka APIs
- [requests](https://pypi.org/project/requests/) for easy downloading of data
- [beautifulsoup4](https://pypi.org/project/beautifulsoup4/) for HTML parsing
- [lxml](https://pypi.org/project/lxml/) as silent dependency for bs4
- [click](https://pypi.org/project/click/) for CLI capabilities

For other dependencies, see [part 4](#4-using-faust-instead-of-kafka-python).

#### 3.3.2. Select some recipes

Select some arbitrary recipe index pages from [allrecipes.com](https://www.allrecipes.com/); examples:

- [Coleslaw recipes](https://www.allrecipes.com/recipes/218/salad/coleslaw/)
- [Pasta Salad recipes](https://www.allrecipes.com/recipes/215/salad/pasta-salad/)
- [Seafood Salad recipes](https://www.allrecipes.com/recipes/408/salad/seafood-salad/)

#### 3.3.3. Run the downloader/dataminer/producer
Using the URLs from 2., run:

- `python3 -m kafka_recipes.main download-raw-recipes --topic recipes-raw --max-count 10 --delay 2 --url 'https://www.allrecipes.com/recipes/218/salad/coleslaw/'`

This will find links to recipes from the given URL and download the first 10 (`--max-count`) of them as HTML, 
storing them into *recipes-raw* Kafka topic.
In order not to spam allrecipes too much, 2 seconds are waited between each download (`--delay`).

While downloading is in progress, you can check the events in *Kafka Tool*.

#### 3.3.4. Run the transformation/parser

- `python3 -m kafka_recipes.main parse-raw-recipes --source-topic recipes-raw --target-topic recipes-parsed`

This will start consuming events from *recipes-raw* and store results into *recipes-parsed* as JSON.

#### 3.3.5. Run the calories alert/monitor

- `python3 -m kafka_recipes.main alert-calories --topic recipes-parsed --calories-threshold 200`

This will start reading events from *recipes-parsed* and writes a message to stdout whenever a recipe with more than 200 calories is found.

#### 3.3.6. Run it all together
Now run them all together at the same time!
Launch multiple `download-raw-recipes` downloaders for misc. recipe pages (possibly omitting the `--max-count` arg) 
and watch as the data are being produced, transformed, and finally consumed by the calory alert.


## 4. Using *Faust* instead of *Kafka-Python*
[Faust](https://faust.readthedocs.io/) is a stream processing library, porting ideas from Kafka Stream to Python.
It is a higher-level API, processing endless streams or events. It also offers serialization of events into predefined
models and *tables* — distributed key/value store (embedded database) to be used across workers.

## 4.1. *faust-recipes* overview

- [Steps 2 and 3](#markdown-header-31-app-overview) were joined into a [single Faust agent](faust_recipes/main.py)
- Steps 1 remains the same — there is no applicable standalone producer pattern in Faust AFAIK.

### 4.2. Running

- `faust -A faust_recipes.main -D /tmp/faust worker`
