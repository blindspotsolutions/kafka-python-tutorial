from typing import List

import faust


class ParsedRecipe(faust.Record):
    title: str
    link: str
    submitter: str
    description: str
    calories: float
    ingredients: List[str]
