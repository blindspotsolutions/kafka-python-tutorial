import faust

from faust_recipes.models import ParsedRecipe
from kafka_recipes.recipes import parse_recipe


class Config:
    group_id = 'faust-tutorial'
    topic_raw = 'recipes-raw'
    topic_parsed = 'recipes-parsed'
    calories_threshold = 200


app = faust.App(
    Config.group_id,
    broker=f'kafka://localhost:9092'
)


topic_recipes_raw = app.topic(Config.topic_raw, value_serializer='raw')
topic_recipes_parsed = app.topic(Config.topic_parsed, value_type=ParsedRecipe)


@app.agent(topic_recipes_raw)
async def parse(stream):
    async for value in stream:
        json = parse_recipe(value)
        parsed_recipe = ParsedRecipe.from_data(json)
        app.log.info(f"Parsed {parsed_recipe.title!r}")
        await topic_recipes_parsed.send(value=parsed_recipe)


@app.agent(topic_recipes_parsed)
async def alert_calories(recipes):
    async for recipe in recipes:
        if recipe.calories > Config.calories_threshold:
            app.log.warn(f"Alert: Recipe {recipe.title!r} has {recipe.calories} calories")
