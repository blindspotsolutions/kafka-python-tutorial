# common
beautifulsoup4==4.9.3
click==7.1.2
lxml==4.5.2
requests==2.24.0

# Kafka-Python
kafka-python==1.4.6

# Faust
faust==1.10.4
multidict==4.7.6
yarl==1.5.0
